package io.jcode.codegen.model.jboot;

import com.jfinal.plugin.activerecord.generator.BaseModelGenerator;

public class JbootBaseModelGenerator extends BaseModelGenerator {

    public JbootBaseModelGenerator(String baseModelPackageName,
                                   String baseModelOutputDir) {
        super(baseModelPackageName, baseModelOutputDir);

        this.template = "jboot/model/base_model_template.jf";

    }
}
