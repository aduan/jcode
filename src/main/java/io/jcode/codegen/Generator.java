package io.jcode.codegen;

public interface Generator {
    void generate();
}
