package io.jcode.codegen.service.jboot;

import io.jcode.codegen.utils.FileUtils;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.BaseModelGenerator;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.jfinal.template.Engine;
import com.jfinal.template.source.ClassPathSourceFactory;

import java.io.File;
import java.io.IOException;

public class JbootServiceImplGenerator extends BaseModelGenerator {
    private String modelPackage;
    private String servicePackage;

    public JbootServiceImplGenerator(String basePackage, String outputDir) {
        super(basePackage + ".service.impl",
                outputDir + File.separator + basePackage.replace(".", File.separator)
                        + File.separator + "service" + File.separator + "impl");
        this.modelPackage = basePackage + ".model";
        this.servicePackage = basePackage + ".service";
        this.template = "jboot/service/service_impl_template.jf";
    }

    @Override
    protected void initEngine() {
        Engine engine = Engine.create("forServiceImpl");
        engine.setSourceFactory(new ClassPathSourceFactory());
        engine.addSharedMethod(new StrKit());
        engine.addSharedObject("getterTypeMap", this.getterTypeMap);
        engine.addSharedObject("javaKeyword", this.javaKeyword);
    }

    @Override
    protected void genBaseModelContent(TableMeta tableMeta) {
        Kv data = Kv.by("serviceImplPackageName", this.baseModelPackageName);
        data.set("generateChainSetter", this.generateChainSetter);
        data.set("tableMeta", tableMeta);
        data.set("servicePackage", this.servicePackage);
        data.set("modelPackage", this.modelPackage);
        Engine engine = Engine.use("forServiceImpl");
        tableMeta.baseModelContent = engine.getTemplate(this.template).renderToString(data);
    }

    @Override
    protected void writeToFile(TableMeta tableMeta) throws IOException {
        String target = this.baseModelOutputDir + File.separator + tableMeta.modelName + "ServiceImpl.java";
        FileUtils.write(target, tableMeta.baseModelContent);
    }
}
