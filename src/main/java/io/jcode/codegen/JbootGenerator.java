package io.jcode.codegen;

import io.jcode.codegen.controller.jboot.JbootRestControllerGenerator;
import io.jcode.codegen.model.jboot.JbootBaseModelGenerator;
import io.jcode.codegen.model.jboot.JbootModelGenerator;
import io.jcode.codegen.service.jboot.JbootServiceImplGenerator;
import io.jcode.codegen.service.jboot.JbootServiceInterfaceGenerator;
import io.jcode.codegen.utils.CodeType;
import io.jcode.codegen.utils.MetaBuilder;
import com.jfinal.plugin.activerecord.generator.TableMeta;

import java.io.File;
import java.util.List;

public class JbootGenerator implements Generator {

    private CodeType codeType;

    private String sourceRootDir;

    private String basePackage;

    private MetaBuilder metaBuilder;

    public JbootGenerator(CodeType codeType, String sourceRootDir, String basePackage, MetaBuilder metaBuilder) {
        this.codeType = codeType;
        this.sourceRootDir = sourceRootDir;
        this.basePackage = basePackage;
        this.metaBuilder = metaBuilder;
    }

    @Override
    public void generate() {
        String modelPackageName = basePackage + ".model";
        String baseModelPackageName = modelPackageName + ".base";

        String modelSourceDir = sourceRootDir + File.separator + modelPackageName.replace(".", File.separator);
        String baseModelSourceDir = sourceRootDir + File.separator + baseModelPackageName.replace(".", File.separator);

        List<TableMeta> tableMetas = metaBuilder.build();

        switch (codeType) {
            case BASE_MODEL:
                new JbootBaseModelGenerator(baseModelPackageName, baseModelSourceDir).generate(tableMetas);
                break;
            case MODEL:
                new JbootModelGenerator(modelPackageName, baseModelPackageName, modelSourceDir).generate(tableMetas);
                break;
            case SERVICE:
                new JbootServiceInterfaceGenerator(basePackage, sourceRootDir).generate(tableMetas);
                new JbootServiceImplGenerator(basePackage, sourceRootDir).generate(tableMetas);
                break;
            case CONTROLLER:
                new JbootRestControllerGenerator(basePackage, sourceRootDir).generate(tableMetas);
                break;
            case ALL:
            default:
                new JbootBaseModelGenerator(baseModelPackageName, baseModelSourceDir).generate(tableMetas);
                new JbootModelGenerator(modelPackageName, baseModelPackageName, modelSourceDir).generate(tableMetas);

                new JbootServiceInterfaceGenerator(basePackage, sourceRootDir).generate(tableMetas);
                new JbootServiceImplGenerator(basePackage, sourceRootDir).generate(tableMetas);

                new JbootRestControllerGenerator(basePackage, sourceRootDir).generate(tableMetas);
                break;
        }
    }
}
