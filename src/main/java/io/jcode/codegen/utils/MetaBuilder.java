package io.jcode.codegen.utils;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.TableMeta;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class MetaBuilder extends com.jfinal.plugin.activerecord.generator.MetaBuilder {
    private String tablePrefix;

    public MetaBuilder(DataSource dataSource) {
        super(dataSource);
    }

    public MetaBuilder(DataSource dataSource, String tablePrefix) {
        this(dataSource);
        this.tablePrefix = tablePrefix;
    }

    @Override
    public List<TableMeta> build() {
        List<TableMeta> tableMetas = super.build();
        if(StrKit.isBlank(this.tablePrefix)) {
            return tableMetas;
        }
        if(tableMetas == null || tableMetas.size() == 0) {
            return tableMetas;
        }
        List<TableMeta> tableMetaList = new ArrayList<>();
//        tableMetas.stream().filter(t -> t.name.startsWith(tablePrefix)).forEach(t -> tableMetaList.add(t));
        for (TableMeta tableMeta : tableMetas) {
            if(tableMeta.name.startsWith(tablePrefix)) {
                tableMetaList.add(tableMeta);
            }
        }
        return tableMetaList;
    }
}
