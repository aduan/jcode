package io.jcode.codegen.utils;

import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public class DbUtils {
    public static ComboPooledDataSource getDatasource(String url, String user, String password) {
        C3p0Plugin c3p0Plugin = new C3p0Plugin(url, user, password);
        c3p0Plugin.start();
        return c3p0Plugin.getComboPooledDataSource();
    }
}
