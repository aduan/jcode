package io.jcode.codegen.utils;

public enum CodeType {
    ALL,
    BASE_MODEL,
    MODEL,
    SERVICE,
    CONTROLLER
}
