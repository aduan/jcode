package io.jcode.codegen.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public final class FileUtils {
    public static void write(String fileName, String content) throws IOException {
        write(fileName, content, false);
    }

    public static void write(String fileName, String content, boolean force) throws IOException {
        File file = new File(fileName);
        if(!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if(force || !file.exists()) {
            FileWriter fw = new FileWriter(file);
            try {
                fw.write(content);
            } finally {
                fw.close();
            }
        }
    }

    public static boolean exists(String fileName) {
        try {
            File file = new File(fileName);
            return file.exists();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }
    }
}
