package io.jcode.codegen.controller.jboot;

import io.jcode.codegen.utils.FileUtils;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.BaseModelGenerator;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.jfinal.template.Engine;
import com.jfinal.template.source.ClassPathSourceFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JbootRestControllerGenerator extends BaseModelGenerator {
    private String basePackage;
    private String modelPackage;
    private String servicePackage;

    private String apiResultFile;

    public JbootRestControllerGenerator(String basePackage, String outputDir) {
        super(basePackage + ".web.controller",
                outputDir + File.separator + basePackage.replace(".", File.separator)
                        + File.separator + "web" + File.separator + "controller");
        this.basePackage = basePackage;
        this.modelPackage = basePackage + ".model";
        this.servicePackage = basePackage + ".service";
        this.template = "jboot/controller/rest_controller_template.jf";

        this.apiResultFile = outputDir + File.separator + basePackage.replace(".", File.separator)
                + File.separator + "web" + File.separator + "ApiResult.java";
    }

    @Override
    public void generate(List<TableMeta> tableMetas) {
        super.generate(tableMetas);

        if(!FileUtils.exists(this.apiResultFile)) {
            this.template = "jboot/controller/apiresult_template.jf";
            Kv data = Kv.by("basePackage", this.basePackage + ".web");
            Engine engine = Engine.use("forRestController");

            String classContent = engine.getTemplate(this.template).renderToString(data);
            try {
                FileUtils.write(apiResultFile, classContent);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void initEngine() {
        Engine engine = Engine.create("forRestController");
        engine.setSourceFactory(new ClassPathSourceFactory());
        engine.addSharedMethod(new StrKit());
        engine.addSharedObject("getterTypeMap", this.getterTypeMap);
        engine.addSharedObject("javaKeyword", this.javaKeyword);
    }

    @Override
    protected void genBaseModelContent(TableMeta tableMeta) {
        Kv data = Kv.by("controllerPackage", this.baseModelPackageName);
        data.set("generateChainSetter", this.generateChainSetter);
        data.set("tableMeta", tableMeta);
        data.set("servicePackage", this.servicePackage);
        data.set("modelPackage", this.modelPackage);
        data.set("apiResultClass", this.basePackage + ".web.ApiResult");
        Engine engine = Engine.use("forRestController");
        tableMeta.baseModelContent = engine.getTemplate(this.template).renderToString(data);
    }

    @Override
    protected void writeToFile(TableMeta tableMeta) throws IOException {
        String target = this.baseModelOutputDir + File.separator + tableMeta.modelName + "RestController.java";
        FileUtils.write(target, tableMeta.baseModelContent);
    }
}
