package io.jcode.codegen;

import io.jcode.codegen.utils.MetaBuilder;

import javax.sql.DataSource;
import java.io.File;

public class JfinalGenerator implements Generator {
    private DataSource dataSource;

    private String sourceRootDir;

    private String basePackage;

    private MetaBuilder metaBuilder;

    public JfinalGenerator(DataSource dataSource, String sourceRootDir, String basePackage) {
        this.dataSource = dataSource;
        this.sourceRootDir = sourceRootDir;
        this.basePackage = basePackage;
    }

    public MetaBuilder getMetaBuilder() {
        return metaBuilder;
    }

    public void setMetaBuilder(MetaBuilder metaBuilder) {
        this.metaBuilder = metaBuilder;
    }

    @Override
    public void generate() {
        String modelPackageName = basePackage + ".model";
        String baseModelPackageName = modelPackageName + ".base";

        String modelSourceDir = sourceRootDir + File.separator + modelPackageName.replace(".", File.separator);
        String baseModelSourceDir = sourceRootDir + File.separator + baseModelPackageName.replace(".", File.separator);
        com.jfinal.plugin.activerecord.generator.Generator generator =
                new com.jfinal.plugin.activerecord.generator.Generator(
                        dataSource,
                        baseModelPackageName,
                        baseModelSourceDir,
                        modelPackageName,
                        modelSourceDir
                );
        generator.setMetaBuilder(metaBuilder);

        generator.generate();

    }
}
